<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
     <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Personne</title>
</head>
<body>
	<form:form method="post" modelAttribute="Update" action="UpdateForm">
		<table border="1">
			<thead>
				<tr>
				<th>ID</th>
				<th>FIRSTNAME</th>
				<th>LASTNAME</th>
				<th>EMAIL</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${UpdateF.listPerson}" var="person" varStatus="status">
				<tr>
				<td>
					<c:out value="${person.id}"/>
					<input type="hidden" name="listPerson[${status.index}].id" value="${person.id}"/>
				</td>
				<td>
					<input type="text" name="listPerson[${status.index}].firstname" value="${person.firstname}"/>
				</td>
				<td>
					<input type="text" name="listPerson[${status.index}].lastname" value="${person.lastname}"/><br/>
				</td>
				<td>
					<input type="text" name="listPerson[${status.index}].email" value="${person.email}"/><br/>
				</td>
				</tr>
				</c:forEach>
			</tbody>
			</table>
			<input type="submit"/>
			</form:form>
</body>
</body>
</html>
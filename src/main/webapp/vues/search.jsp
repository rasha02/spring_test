<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
     <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Personne</title>
</head>
<body>


	 <body>
        <form:form method="post" modelAttribute="searchPerson" action="searchForm">
              First Name
            <form:input path="FIRST"/>
            <b><i><form:errors path="FIRST" cssclass="error"/></i></b><br>
            <input type="submit"/>
        </form:form>

	
		
		
		
	<table border="1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Email</th>
			<th>First Name</th>
			<th>Last Name</th>
		</tr>
		</thead>
		<tbody>
			<c:forEach items="${SP}" var="person">
				<tr>
					<td><c:out value="${person.id}"/></td>
					<td><c:out value="${person.email}"/></td>
					<td><c:out value="${person.firstname}"/></td>
					<td><c:out value="${person.lastname}"/></td>
				</tr>
			</c:forEach>
		</tbody>
		
	</table>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
     <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Personne</title>
</head>
<body>
		
	<table border="1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Email</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
			<c:forEach items="${LIST}" var="person">
				<tr>
					<td><c:out value="${person.id}"/></td>
					<td><c:out value="${person.email}"/></td>
					<td><c:out value="${person.firstname}"/></td>
					<td><c:out value="${person.lastname}"/></td>
					<td>
						<c:url value="/deletePerson" var="url">
							<c:param name="idPerson" value="${person.id}"/>
						</c:url>
						<a href="${url}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
		
	</table>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Person</title>
</head>
<body>
	<form action="addPerson" method="GET" modelAttribute="addPersonData">
	
     <input type="text" placeholder="Email" required autofocus name="email">

     <input type="text" placeholder="Firstname" required name="firstname">
     
     <input type="text" placeholder="Lastname" required name="lastname">

     <input type="submit" value="Insert">
</form>
</body>
</html>
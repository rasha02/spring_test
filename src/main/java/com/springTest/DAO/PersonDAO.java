package com.springTest.DAO;

import java.util.List;

import com.springTest.Entity.Person;

public interface PersonDAO {
	void add(Person person);
	List<Person> listPerson();
	List<Person> Search(String name);
	List<Person> SearchWTChar(String name);
	void deletePerson(final Person person);
	void updatePerson(final Person person);
	Person findPersonById (int id);
	
}

package com.springTest.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.springTest.Entity.Person;

@Repository
public class PersonDAOImpl implements PersonDAO {

	@PersistenceContext
	private EntityManager em;
	
	public void add(Person person) {
		em.persist(person);
	}

	public List<Person> listPerson() {
		CriteriaBuilder CB = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = CB.createQuery(Person.class);
		@SuppressWarnings("unused")
		Root<Person> root = criteriaQuery.from(Person.class);
		return em.createQuery(criteriaQuery).getResultList();
	}

	public List<Person> Search(String name) {
		CriteriaBuilder CB = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = CB.createQuery(Person.class);
		@SuppressWarnings("unused")
		Root<Person> root = criteriaQuery.from(Person.class);
		Expression<String> path = root.get("firstname");
		criteriaQuery.where(CB.equal(root.get("firstname"),name));
		
		return em.createQuery(criteriaQuery).getResultList();
	}
	
	public List<Person> SearchWTChar(String name)
	{
		CriteriaBuilder CB = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = CB.createQuery(Person.class);
		@SuppressWarnings("unused")
		Root<Person> root = criteriaQuery.from(Person.class);
		Expression<String> path = root.get("firstname");
		criteriaQuery.where(CB.like(path,"%"+name+"%"));
		return em.createQuery(criteriaQuery).getResultList();
	}

	public void deletePerson(final Person person)
	{
		final Person Pperson = em.getReference(Person.class, person.getId());
		em.remove(Pperson);
	}
	
	public void updatePerson(final Person person)
	{
		final CriteriaBuilder CB = em.getCriteriaBuilder();
		final CriteriaUpdate<Person> criteriaUpdate = CB.createCriteriaUpdate(Person.class);
		final Root<Person> root = criteriaUpdate.from(Person.class);
		final Path<Person> path = root.get("id");
		final Expression<Boolean> expression= CB.equal(path, person.getId());
		criteriaUpdate.where(expression);
		criteriaUpdate.set("email", person.getEmail());
		criteriaUpdate.set("firstname", person.getFirstname());
		criteriaUpdate.set("lastname",person.getLastname());
		final Query query=em.createQuery(criteriaUpdate);
		final int RowCount = query.executeUpdate();
		
		if(RowCount != 1)
		{
			System.out.println("ERROR");
		}
		else
		{
			System.out.println(person.getEmail());
			System.out.println(person.getFirstname());
			System.out.println(person.getLastname());
		}
		
	}

	public Person findPersonById(int id) {
		CriteriaBuilder CB = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = CB.createQuery(Person.class);
		@SuppressWarnings("unused")
		Root<Person> root = criteriaQuery.from(Person.class);
		Expression<String> path = root.get("id");
		criteriaQuery.where(CB.equal(root.get("id"),id));
		
		return em.createQuery(criteriaQuery).getSingleResult();
	}
}


package com.springTest.Controllers;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

//import org.hibernate.validator.constraints.NotEmpty;

public class PersonForm {

	//@NotEmpty
	@NotNull
	private String FIRST;
	//@NotEmpty
	@NotNull
	private String LAST;
	//@NotEmpty
	@NotNull
	@Pattern(regexp="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$")
	private String EMAIL;
	public String getFIRST() {
		return FIRST;
	}
	public void setFIRST(String fIRST) {
		FIRST = fIRST;
	}
	public String getLAST() {
		return LAST;
	}
	public void setLAST(String lAST) {
		LAST = lAST;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	
	
}

package com.springTest.Controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/formation/{Name}")
public class FormationController {

	@RequestMapping(method =RequestMethod.GET)
	public String afficheFormation(final ModelMap pModel,@PathVariable(value="Name") final String Name)
	{
		pModel.addAttribute("Name",Name);
		pModel.addAttribute("formation","J2EE");
		return "formation";
	}
	
}

package com.springTest.Controllers;

import java.util.List;

public class PersonUpdateForm {

	private List<PersonUpdate> listPerson;
	
	public void setListPerson(final List<PersonUpdate> lp)
	{
		listPerson = lp;
	}
	
	public List<PersonUpdate> getListPerson()
	{
		return listPerson;
	}
	
}

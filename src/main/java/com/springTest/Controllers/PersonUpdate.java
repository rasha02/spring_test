package com.springTest.Controllers;

public class PersonUpdate {
	private int id;
	private String email;
	private String firstname;
	private String lastname;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String eMAIL) {
		email = eMAIL;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String fIRST_NAME) {
		firstname = fIRST_NAME;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lAST_NAME) {
		lastname = lAST_NAME;
	}
	
	
	
}

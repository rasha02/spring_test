package com.springTest.Controllers;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springTest.Entity.Person;
import com.springTest.Service.PersonService;

@Controller
public class PersonneController {
	
	@Autowired
	private PersonService personServiceImpl;
	@RequestMapping(value="/personne",method=RequestMethod.GET)
		public String getAllPersonne(ModelMap pModel)
		{
			final List<Person> listPerson = personServiceImpl.listPerson();
			pModel.addAttribute("listPerson",listPerson);
			if(pModel.get("creation")== null)
			{
				pModel.addAttribute("addPersonData",new PersonForm());
			}
			return "personne";
		}
	

	@RequestMapping(value="/addPerson", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("addPersonData") PersonForm p,final BindingResult pBindingResult,final ModelMap pModel)
	{
		if(!pBindingResult.hasErrors())
		{
			String name = p.getFIRST();
			String last= p.getLAST();
			String email =p.getEMAIL();
			
			Person person = new Person(email,name,last);
			
			personServiceImpl.add(person);
		}
		return getAllPersonne(pModel);
	}
	
	
	
	
	
	/*---------------------------------------------------------------*/
	
	
	@RequestMapping(value="/search",method=RequestMethod.GET)
	public String getsearchPerson(ModelMap pModel,String name)
	{
		final List<Person> searchPerson = personServiceImpl.Search(name);
		pModel.addAttribute("SP",searchPerson);
		if(pModel.get("searchPerson")== null)
		{
			pModel.addAttribute("searchPerson",new SearchForm());
		}
		return "search";
	}
	
	@RequestMapping(value="/searchForm", method = RequestMethod.POST)
	public String searchPerson(@ModelAttribute("searchPerson") SearchForm p,final BindingResult pBindingResult,final ModelMap pModel)
	{
		String name = "";
		if(!pBindingResult.hasErrors())
		{
			 name= p.getFIRST();
			System.out.println(name);
			personServiceImpl.Search(name);
		}
		return getsearchPerson(pModel,name);
	}
	
	
	
	
	
	
	/*---------------------------Search By a Char------------------------------------*/
	
	
	@RequestMapping(value="/search-by-char",method=RequestMethod.GET)
	public String getsearchPersonChar(ModelMap pModel,String name)
	{
		final List<Person> searchPerson = personServiceImpl.SearchWTChar(name);
		pModel.addAttribute("SPCHAR",searchPerson);
		if(pModel.get("searchPersonChar")== null)
		{
			pModel.addAttribute("searchPersonChar",new SearchForm());
		}
		return "search-by-char";
	}
	
	@RequestMapping(value="/searchByChar", method = RequestMethod.POST)
	public String searchPersonChar(@ModelAttribute("searchPersonChar") SearchForm p,final BindingResult pBindingResult,final ModelMap pModel)
	{
		String name = "";
		if(!pBindingResult.hasErrors())
		{
			 name= p.getFIRST();
			System.out.println(name);
			personServiceImpl.SearchWTChar(name);
		}
		return getsearchPersonChar(pModel,name);
	}
	
	
	
	
	/*-----------------------------------------------------------------*/
	
	
	@RequestMapping(value="/ShowPersonListForDeletion",method=RequestMethod.GET)
	public String showPersonList(ModelMap pModel)
	{
		final List<Person> searchPerson = personServiceImpl.listPerson();
		pModel.addAttribute("LIST",searchPerson);
		return "delete";
	}
	
	@RequestMapping(value="/deletePerson", method = RequestMethod.GET)
	public String DeletePerson(@RequestParam(value="idPerson") final int idPerson,final ModelMap pModel)
	{
		personServiceImpl.deletePerson(idPerson);
		return showPersonList(pModel);
	}
	
	
	
	/*-------------------------------------------------------------------------*/
	
	@RequestMapping(value="/ShowUpdatePerson",method=RequestMethod.GET)
	public String showUpdateForm(final ModelMap pModel)
	{
		if(pModel.get("updatePerson")== null)
		{
			final List<Person> listPerson = personServiceImpl.listPerson();
			final PersonUpdateForm updateForm = new PersonUpdateForm();
			final List<PersonUpdate> List = new LinkedList<PersonUpdate>();
			for(final Person person :listPerson)
			{
				final PersonUpdate pu = new PersonUpdate();
				pu.setId(person.getId());
				pu.setEmail(person.getEmail());
				pu.setFirstname(person.getFirstname());
				pu.setLastname(person.getLastname());
				List.add(pu);
			}
			updateForm.setListPerson(List);
			pModel.addAttribute("UpdateF",updateForm);
		}
		return "updatePerson";
	}
	
	@RequestMapping(value="/UpdateForm",method=RequestMethod.POST)
	public String UpdatePerson(@ModelAttribute(value="Update")final PersonUpdateForm PUF,final BindingResult bindingResult,final ModelMap pModel)
	{
		if(!bindingResult.hasErrors())
		{
			final List<Person> listPerson = new LinkedList<Person>();
			for(final PersonUpdate person : PUF.getListPerson())
			{
				final Person p = new Person();
				p.setId(person.getId());
				p.setEmail(person.getEmail());
				p.setFirstname(person.getFirstname());
				p.setLastname(person.getLastname());
				listPerson.add(p);
			}
			personServiceImpl.updatePerson(listPerson);
		}
		
		return showUpdateForm(pModel);
	}
}

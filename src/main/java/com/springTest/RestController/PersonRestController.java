package com.springTest.RestController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.springTest.Entity.Person;
import com.springTest.Service.PersonService;
import com.springTest.Service.PersonServiceImpl;

@RestController
public class PersonRestController {

@Autowired
private PersonService servicePerson;

/*-----------------------------------------------------------------*/

@RequestMapping(value= "/user/", method= RequestMethod.GET)
public ResponseEntity<List<Person>> ListAllUsers() {
	
	List<Person> listPerson= servicePerson.listPerson();
	if(listPerson.isEmpty())
	{
		return new ResponseEntity<List<Person>>(HttpStatus.NO_CONTENT);
	}
	else
		return new ResponseEntity<List<Person>>(listPerson,HttpStatus.OK);
	
}

/*------------------------------GET-----------------------------------*/
@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Person> getUser(@PathVariable("id") int id) {
    System.out.println("Fetching User with id "+ id);
    Person user = servicePerson.findPersonById(id);
    if(user == null) {
        System.out.println("User with id "+ id + " not found");
        return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<Person>(user, HttpStatus.OK);
}

/*-----------------------------CREATE------------------------------------*/

@RequestMapping(value = "/user/", method = RequestMethod.POST)
public ResponseEntity<Void>createUser(@RequestBody Person person, UriComponentsBuilder ucBuilder) {
   System.out.println("Creating User "+ person.getFirstname());

   if(servicePerson.Search(person.getFirstname()).size()>0) {
       System.out.println("A User with name "+ person.getFirstname() + " already exist");
       return new ResponseEntity<Void>(HttpStatus.CONFLICT);
   }

   servicePerson.add(person);

   HttpHeaders headers = new HttpHeaders();
   headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(person.getId()).toUri());
   return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
}

/*-----------------------------UPDATE------------------------------------*/

@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
public ResponseEntity<Person> updateUser(@PathVariable("id") int id, @RequestBody Person person) {
    System.out.println("Updating User "+ id);
     
    Person currentUser = servicePerson.findPersonById(id);
     
    if(currentUser==null) {
        System.out.println("User with id "+ id + " not found");
        return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
    }

    currentUser.setFirstname(person.getFirstname());
    currentUser.setLastname(person.getLastname());
    currentUser.setEmail(person.getEmail());
     
    servicePerson.updateOnePerson(currentUser);
    return new ResponseEntity<Person>(currentUser, HttpStatus.OK);
}

/*---------------------------DELETE--------------------------------------*/

@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
public ResponseEntity<Person> deleteUser(@PathVariable("id") int id) {
    System.out.println("Fetching & Deleting User with id "+ id);

    Person user = servicePerson.findPersonById(id);
    if(user == null) {
        System.out.println("Unable to delete. User with id "+ id + " not found");
        return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
    }

    servicePerson.deletePerson(id);
    return new ResponseEntity<Person>(HttpStatus.NO_CONTENT);
}




}

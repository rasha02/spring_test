package com.springTest.Service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springTest.DAO.PersonDAO;
import com.springTest.Entity.Person;


@Service
public class PersonServiceImpl implements PersonService{

	@Autowired
	private PersonDAO personDao;
	
	@Transactional
	public void add(Person person) {
		
		personDao.add(person);
	}
	
	

	@Transactional(readOnly = true)
	public List<Person> listPerson() {
		
		return personDao.listPerson();
	}
	
	@Transactional
	public List<Person> Search(String name)
	{
		return personDao.Search(name);
	}
	
	@Transactional
	public List<Person> SearchWTChar(String name)
	{
		return personDao.SearchWTChar(name);
	}
	
	@Transactional
	public void deletePerson(final int idPerson)
	{
		final Person person = new Person();
		person.setId(idPerson);
		personDao.deletePerson(person);
	}
	@Transactional
	public void updatePerson(final List<Person> PersonList)
	{
		for(final Person person : PersonList)
		{
			personDao.updatePerson(person);
		}
	}
	@Transactional
	public Person findPersonById (int id)
	{
		return personDao.findPersonById(id);
	}


	@Transactional
	public void updateOnePerson(Person person) {
		personDao.updatePerson(person);
		
	}

}

package com.springTest.Service;

import java.util.List;

import com.springTest.Entity.Person;

public interface PersonService {
	void add(Person person);
	List<Person> listPerson();
	List<Person> Search(String name);
	List<Person> SearchWTChar(String name);
	void deletePerson(final int person);
	void updatePerson(final List<Person> PersonList);
	void updateOnePerson(final Person person);
	Person findPersonById (int id);
}
